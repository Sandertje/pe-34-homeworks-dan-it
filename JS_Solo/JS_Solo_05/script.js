/**
 * При наведенні на блок 1 робити
 * блок 2 зеленим кольором
 * А при наведенні на блок 2 робити
 * блок 1 червоним кольором
 *
 */

const elem1 = document.getElementById("block-1");
const elem2 = document.getElementById("block-2");

elem1.addEventListener("mouseover", () => {
  elem2.style.background = "green";});

elem1.addEventListener("mouseout", () => {
  elem2.style.background = "";});

elem2.addEventListener("mouseover", () => {
  elem1.style.background = "red";});

elem2.addEventListener("mouseout", () => {
  elem1.style.background = "";});
