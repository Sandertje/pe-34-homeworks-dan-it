/**
 * Задание 4.
 *
 * Улучшить скрипт из предыдущего задания.
 *
 * При каждом движении курсора по странице
 * менять цвет каждой буквы элемента h1 на случайный.
 */

/* Дано */
const PHRASE = 'Добро пожаловать!';

function getRandomColor() {
  const r = Math.floor(Math.random() * 255);
  const g = Math.floor(Math.random() * 255);
  const b = Math.floor(Math.random() * 255);

  return `rgb(${r}, ${g}, ${b})`;
}

/* Решение */
const text = document.createElement("h1");
text.innerText = PHRASE;
document.body.append(text);

const button = document.createElement("button");
button.innerText = "Раскрасить";
document.body.appendChild(button);

document.body.addEventListener("mousemove", () => {
  const Ababa = text.innerText;
  text.innerText = ``;

  for (let char of Ababa) {
    const span = document.createElement("span");

    span.innerText = char;
    span.style.color = getRandomColor();
    text.append(span)
  }
})
