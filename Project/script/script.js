const servicesPics = Array.from(document.querySelectorAll(".content-services-block-second-img"));
const servicesText = Array.from(document.querySelectorAll(".content-services-block-second-text"));
const servicesMenu = document.querySelector(".content-services-block-menu");
const servicesMenuChildren = Array.from(servicesMenu.children);

servicesMenu.addEventListener("click", (e) =>{
  let target = e.target;
  changeText(target);
})

const changeText = (i) => {
  servicesMenuChildren.forEach((e) => {
    e.classList.remove("content-services-block-menu-item-active");
  });
  i.classList.add("content-services-block-menu-item-active")

  servicesPics.forEach( (e) => {
    if (e.dataset.valueImg !== i.innerText) {
      e.classList.add("hide")
    } else {
      e.classList.remove("hide")
    }
  });

  servicesText.forEach((e) =>{
    if (e.dataset.valueText !== i.innerText) {
      e.classList.add("hide")
    } else {
      e.classList.remove("hide")
    }
  })
}


const buttonLoadMore = document.querySelector(".content-presentation-button");
const amazingWorkBlock = document.querySelector(".content-presentation-block");

buttonLoadMore.addEventListener("click", () => {
  addNewPic(AddedImagesAmazingWork);
  buttonLoadMore.classList.add("hide");
});

const addNewPic = (i) => {
    const htmlPic = i.map((element) => {
    return `<div class="content-presentation-block-item">
               <img class="content-presentation-block-item-pic" src="${element.src}" alt="">
               <div class="content-presentation-block-item-information">
                   <img class="content-presentation-block-item-information-svg" src="img/Amazing-work-hover-svg.svg" alt="">
                   <p class="content-presentation-block-item-information-meaning">creative design</p>
                   <p class="content-presentation-block-item-information-position">Web Design</p>
               </div>
           </div>`
  });
  amazingWorkBlock.insertAdjacentHTML("beforeend", `${htmlPic.join("")}`);
}

const AddedImagesAmazingWork = [
  {
    src: 'img/Our Amazing Work pic/1.jpg'
  },
  {
    src: 'img/Our Amazing Work pic/2.jpg'
  },
  {
    src: 'img/Our Amazing Work pic/3.jpg'
  },
  {
    src: 'img/Our Amazing Work pic/4.jpg'
  },
  {
    src: 'img/Our Amazing Work pic/5.jpg'
  },
  {
    src: 'img/Our Amazing Work pic/6.jpg'
  },
  {
    src: 'img/Our Amazing Work pic/7.jpg'
  },
  {
    src: 'img/Our Amazing Work pic/8.jpg'
  },
  {
    src: 'img/Our Amazing Work pic/9.jpg'
  },
  {
    src: 'img/Our Amazing Work pic/10.jpg'
  },
  {
    src: 'img/Our Amazing Work pic/11.jpg'
  },
  {
    src: 'img/Our Amazing Work pic/12.jpg'
  },
]

const AllImagesAmazingWork = [
  {
    src: 'img/graphic design/graphic-design1.jpg',
    category: "Graphic Design"
  },
  {
    src: 'img/graphic design/graphic-design2.jpg',
    category: "Graphic Design"
  },
  {
    src: 'img/graphic design/graphic-design3.jpg',
    category: "Graphic Design"
  },
  {
    src: 'img/graphic design/graphic-design4.jpg',
    category: "Graphic Design"
  },
  {
    src: 'img/graphic design/graphic-design5.jpg',
    category: "Graphic Design"
  },
  {
    src: 'img/graphic design/graphic-design6.jpg',
    category: "Graphic Design"
  },
  {
    src: 'img/graphic design/graphic-design7.jpg',
    category: "Graphic Design"
  },
  {
    src: 'img/graphic design/graphic-design8.jpg',
    category: "Graphic Design"
  },
  {
    src: 'img/graphic design/graphic-design9.jpg',
    category: "Graphic Design"
  },
  {
    src: 'img/graphic design/graphic-design10.jpg',
    category: "Graphic Design"
  },
  {
    src: 'img/graphic design/graphic-design11.jpg',
    category: "Graphic Design"
  },
  {
    src: 'img/graphic design/graphic-design12.jpg',
    category: "Graphic Design"
  },
  {
    src: 'img/web design/web-design1.jpg',
    category: "Web Design"
  },
  {
    src: 'img/web design/web-design2.jpg',
    category: "Web Design"
  },
  {
    src: 'img/web design/web-design3.jpg',
    category: "Web Design"
  },
  {
    src: 'img/web design/web-design4.jpg',
    category: "Web Design"
  },
  {
    src: 'img/web design/web-design5.jpg',
    category: "Web Design"
  },
  {
    src: 'img/web design/web-design6.jpg',
    category: "Web Design"
  },
  {
    src: 'img/web design/web-design7.jpg',
    category: "Web Design"
  },
  {
    src: 'img/landing page/landing-page1.jpg',
    category: "Landing Pages"
  },
  {
    src: 'img/landing page/landing-page2.jpg',
    category: "Landing Pages"
  },
  {
    src: 'img/landing Page/landing-page3.jpg',
    category: "Landing Pages"
  },
  {
    src: 'img/landing Page/landing-page4.jpg',
    category: "Landing Pages"
  },
  {
    src: 'img/landing Page/landing-page5.jpg',
    category: "Landing Pages"
  },
  {
    src: 'img/landing Page/landing-page6.jpg',
    category: "Landing Pages"
  },
  {
    src: 'img/landing Page/landing-page7.jpg',
    category: "Landing Pages"
  },
  {
    src: 'img/wordpress/wordpress1.jpg',
    category: "Wordpress"
  },
  {
    src: 'img/wordpress/wordpress2.jpg',
    category: "Wordpress"
  },
  {
    src: 'img/wordpress/wordpress3.jpg',
    category: "Wordpress"
  },
  {
    src: 'img/wordpress/wordpress4.jpg',
    category: "Wordpress"
  },
  {
    src: 'img/wordpress/wordpress5.jpg',
    category: "Wordpress"
  },
  {
    src: 'img/wordpress/wordpress6.jpg',
    category: "Wordpress"
  },
  {
    src: 'img/wordpress/wordpress7.jpg',
    category: "Wordpress"
  },
  {
    src: 'img/wordpress/wordpress8.jpg',
    category: "Wordpress"
  },
  {
    src: 'img/wordpress/wordpress9.jpg',
    category: "Wordpress"
  },
  {
    src: 'img/wordpress/wordpress10.jpg',
    category: "Wordpress"
  },
]

const FilterImages = (arr, category) => arr.filter((e)=> e.category === category);
const AddPic = (arr) => {
  const htmlPic = arr.map((element) => {
    return `<div class="content-presentation-block-item">
               <img class="content-presentation-block-item-pic" src="${element.src}" alt="">
               <div class="content-presentation-block-item-information">
                   <img class="content-presentation-block-item-information-svg" src="img/Amazing-work-hover-svg.svg" alt="">
                   <p class="content-presentation-block-item-information-meaning">creative design</p>
                   <p class="content-presentation-block-item-information-position">Web Design</p>
               </div>
           </div>`
  });
  amazingWorkBlock.innerHTML = htmlPic.join("");
}

const TabsAmazingWork = document.querySelector(".content-presentation-menu");

TabsAmazingWork.addEventListener("click", (e) => {
  Array.from(TabsAmazingWork.children).forEach(e => {
    e.classList.remove("content-presentation-menu-item-active");
  });
  e.target.classList.add("content-presentation-menu-item-active");

   let currentCategory = e.target.dataset.category;
  if (currentCategory === "All" && e.target !== e.currentTarget) {
    AddPic(AllImagesAmazingWork);
  } else if (e.target !== e.currentTarget) {
    const newArr = FilterImages(AllImagesAmazingWork, currentCategory);
    AddPic(newArr);
  }
})

const blockInformation = document.querySelector(".content-about-block-information");
const createAndAdd = (arr) => {
  let newHtml = arr.map((e) => {
    return `<p class="content-about-block-information-text">${e.text}</p>
            <p class="content-about-block-information-name">${e.name}</p>
            <p class="content-about-block-information-position">${e.position}</p>
            <img class="content-about-block-img" src="${e.img}" alt="">`
  })
  blockInformation.classList.add("fade-out")
  setTimeout(()=> {
    blockInformation.classList.remove("fade-out");
    blockInformation.innerHTML = newHtml.join("");
    blockInformation.classList.add("fade-in");
    setTimeout(()=>{
      blockInformation.classList.remove("fade-in");
    },1500);
  }, 1500)
}


const aboutPeople = [
  {
    name: "Miss Janise",
    position: "JS Developer",
    text: "It was just an accident, he said, putting his hand to his cheek. He night, in a calm, with a heavy sea running, the main-boom-lift carried away, an’ next the tackle. The lift was wire, an’ it was threshin’ around like a snake. The whole watch was tryin’ to grab it, an’ I rushed in an’ got swatted. e-reading.club",
    img: "img/Imgs people/people-1.png",
    index: 0,
  },
  {
    name: "Martin Iden",
    position: "Java Developer",
    text: "And thereat she began to talk quickly and easily upon the subject he had suggested. He felt better, and settled back slightly from the edge of the chair, holding tightly to its arms with his hands, as if it might get away from him and buck him to the floor. He had succeeded in making her talk her talk, and while she rattled on, e-reading.club",
    img: "img/Imgs people/people-2.png",
    index: 1,
  },
  {
    name: "Hasan Ali",
    position: "UX Designer",
    text: "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
    img: "img/Imgs people/people-3.png",
    index: 2,
  },
  {
    name: "Mersedes Adelar" ,
    position: "Python Developer" ,
    text: "It sounded like a threat. His voice was determined, his eyes were flashing, the lines of his face had grown harsh. And to her it seemed that the angle of his jaw had changed; its pitch had become unpleasantly aggressive. At the same time a wave of intense virility seemed to surge out from him and impinge upon her. e-reading.club",
    img: "img/Imgs people/people-4.png",
    index: 3,
  }
]

$(document).ready( function (){
  $(`.slider`).slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1
  })
});

const sliderItems = document.querySelector(".slider");

sliderItems.addEventListener("click", (e) => {
  let target = e.target;
  changeInfoAboutPersons(target);
});

const changeInfoAboutPersons = (target) => {
  if (target.classList.contains("slick-prev") || (target.classList.contains("slick-next"))) {
      let activeSlideName = document.querySelector(".slick-current img").dataset.name;
      let newArr = aboutPeople.filter((e)=> e.name === activeSlideName);
      createAndAdd(newArr);
    } else if (target.classList.contains("slider-item-img")) {
      let newArr = aboutPeople.filter((e)=> e.name === target.dataset.name);
      createAndAdd(newArr);
  }
}


