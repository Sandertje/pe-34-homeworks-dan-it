import Button from "./Button";
import {render} from "@testing-library/react"

const onClick = jest.fn();

describe("Button render", () => {
  test("should button render", ()=> {
    const {asFragment} = render( <Button onClick={onClick} text={"Random button text"}/> );
    expect(asFragment()).toMatchSnapshot();
  })
})

describe("Button click", () => {
  test("should button click working", ()=> {
    const {getByText} = render( <Button onClick={onClick} text={"Random button text"}/> );
    const button = getByText("Random button text");
    button.click();
    expect(onClick).toBeCalled();
  })
})

