import {TextField} from "@mui/material";
import {useField} from "formik";
import styles from "../FormInput/formInput.module.scss"

const FormInput = (props) => {
  const [field, meta, helpers] = useField(props);
  const {label, type} = props;

  const isError = meta.touched && meta.error

  return (
    <>
      <TextField className={styles.input} {...field} label={label} type={type} color={isError ? "error" : "primary"}/>
      <span className={styles.error}>{isError ? meta.error : ""}</span>
    </>
  )

}

export default FormInput;
