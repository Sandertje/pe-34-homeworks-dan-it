import Modal from "./Modal";
import {getByTestId, render} from "@testing-library/react";
import {Provider} from 'react-redux';
import {createStore} from "redux";
import reducer from "../../store/reducer";

const renderWithRedux = (
  component,
  {initialValues, store = createStore(reducer, initialValues)} = {}
) =>  {
  return {
    ...render(<Provider store={store}> {component} </Provider>),
    store,
  }
}

describe("Snapshot test", () => {

  test("should Modal render with isOpen false", () => {

    const {asFragment} = renderWithRedux(<Modal/>, {
      initialValues: {
        modal: {
          isOpen: false,
          isHasAddAction: true,
          modalTitle: "Some modal Title",
          id: 1,
        },
        products: [],
      }
    });
    expect(asFragment()).toMatchSnapshot();
  })

    test("should Modal render with isOpen true", () => {

      const {asFragment} = renderWithRedux(<Modal/>, {
        initialValues: {
          modal: {
            isOpen: true,
            isHasAddAction: true,
            modalTitle: "Some modal Title",
            id: 1,
          },
          products: [],
        }
      });
      expect(asFragment()).toMatchSnapshot();
    })
    test("should Modal close after click button-close", () => {
      const {getByText, asFragment, getByTestId} = renderWithRedux(<Modal/>, {
        initialValues: {
          modal: {
            isOpen: true,
            isHasAddAction: true,
            modalTitle: "Some modal Title",
            id: 1,
          },
          products: [],
        }
      });

      getByText("Cancel").click();
      expect(asFragment()).toMatchSnapshot();
    })
  }
)

