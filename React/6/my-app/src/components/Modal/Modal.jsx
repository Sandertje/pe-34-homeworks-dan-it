import styles from './modal.module.scss';
import {useDispatch, useSelector} from "react-redux";
import {setIsModalOpen} from "../../store/actionsCreator/modalCreator";
import {addItemToTheCart, removeItemFromTheCart} from "../../store/actionsCreator/cartCreator";
import Button from "../Button/Button";

function Modal() {
  const {isOpen, isHasAddAction, modalTitle, id} = useSelector(state => state.modal);
  const {posts} = useSelector(state => state.products);
  const dispatch = useDispatch();

  const closeModal = () => dispatch(setIsModalOpen(false));

  const confirmModal = () => {

    if (isHasAddAction) {
      const currentIndex = posts.findIndex(element => element.id === id);
      const {name, price, productCode, color, productImg, count} = posts[currentIndex];
      dispatch(addItemToTheCart({name, price, productCode, color, productImg, count, id}));
    } else {
      dispatch(removeItemFromTheCart(id));
    }

    closeModal();
  }

  if (!isOpen) return null;

    return (
      <div data-testid="container" className={styles.modal}>
        <div className={styles.container}>
          <div className={styles.closeButtonWrapper}>
            <button onClick={closeModal} >x</button>
          </div>
          <div className={styles.contentWrapper}>
            <p className={styles.contentWrapperTitle}>{modalTitle}</p>
          </div>
          <div className={styles.buttonWrapper}>
            <Button onClick={confirmModal} text={"Ok"}/>
            <Button onClick={closeModal} text={"Cancel"}>Cancel</Button>
          </div>
        </div>
        <div onClick={closeModal} className={styles.background}/>
      </div>
    );

}

export default Modal;
