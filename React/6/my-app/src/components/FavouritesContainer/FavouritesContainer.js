import styles from "./favouritesContainer.module.scss"
import Item from "../Item/Item";
import {shallowEqual, useSelector} from "react-redux";


function FavouriteContainer () {
  const {posts} = useSelector(state => state.products, shallowEqual);

  return (
    <section className={styles.container}>
      <div className={styles.containerContent}>
        {posts && posts.map(item =>
          item.isFavourite &&
          <Item key={item.productCode}
                         {...item}
          />
        )
        }
      </div>
    </section>
  );
}

export default FavouriteContainer;
