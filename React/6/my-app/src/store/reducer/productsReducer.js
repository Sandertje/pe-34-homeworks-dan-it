import {GET_DATA, SWITCH_FAVOURITE, GET_DATA_FROM_LS} from "../actions/productsActions";

const initialState = {
  posts: [],
};

export const productsReducer = (state = initialState, action) => {
  switch (action.type){
    case GET_DATA:
      return {...state, posts: action.payload};
    case SWITCH_FAVOURITE:
      const newState = [...state.posts];
      const index = newState.findIndex(element => element.id === action.payload);
      newState[index].isFavourite = !newState[index].isFavourite;
      localStorage.setItem("favorites", JSON.stringify(newState));
      return {...state, posts: newState};
    case GET_DATA_FROM_LS:
      return {...state, posts: action.payload};
    default:{
      return state;
    }
  }
}
