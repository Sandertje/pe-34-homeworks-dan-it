import {GET_DATA, SWITCH_FAVOURITE, GET_DATA_FROM_LS} from "../actions/productsActions";

export const getProductsData = () => async (dispatch) => {
  const result = await fetch('./Posts.json')
    .then(response => response.json());
  dispatch({type: GET_DATA, payload: result});
};

export const changeFavouriteState = (id) => ({
  type: SWITCH_FAVOURITE,
  payload: id,
});

export const getProductsDataFromLS = (data) => ({
  type: GET_DATA_FROM_LS,
  payload: data,
});
