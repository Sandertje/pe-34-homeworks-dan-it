import {SET_IS_MODAL_OPEN, SET_MODAL_PARAMS} from "../actions/modalActions"

export const setIsModalOpen = (value) => ({type: SET_IS_MODAL_OPEN, payload: value});
export const setModalParams = (values) => ({type: SET_MODAL_PARAMS, payload: values});
