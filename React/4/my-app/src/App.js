import Header from "./components/Header/Header";
import { BrowserRouter } from "react-router-dom";
import Routes from "./routes/Routes";
import './App.module.scss';
import {Provider} from "react-redux";
import store from "../src/store/store"
import Modal from "./components/Modal/Modal";


function App() {

  return (
    <Provider store={store}>
      <BrowserRouter>
        <div className="App">
          <Header/>
          <Routes
          />
          <Modal/>
        </div>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
