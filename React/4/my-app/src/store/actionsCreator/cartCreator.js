import {ADD_ITEM, REMOVE_ITEM, GET_CART_ITEM_FROM_LS} from "../actions/cartActions";

export const addItemToTheCart = (cartItem) => ({
  type: ADD_ITEM,
  payload: cartItem,
});

export const removeItemFromTheCart = (id) => ({
  type: REMOVE_ITEM,
  payload: id,
});

export const getCartItemsFromLS = (item) => ({
  type: GET_CART_ITEM_FROM_LS,
  payload: item,
});


