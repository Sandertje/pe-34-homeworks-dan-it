export const ADD_ITEM = 'ADD_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const GET_CART_ITEM_FROM_LS = 'GET_CART_ITEM_FROM_LS';
