import styles from './cartItem.module.scss';
import {useDispatch} from "react-redux";
import {setIsModalOpen, setModalParams} from "../../store/actionsCreator/modalCreator";

const CartItem = ({name, price, productCode, color, productImg, count, id}) => {

  const dispatch = useDispatch();

  const handleDelete = () => {
    dispatch(setModalParams({
      isHasAddAction: false,
      id,
      modalTitle: `Do you really want to delete ${name} from your cart?`,
      name,
      })
    )
    dispatch(setIsModalOpen(true));
  }

  return (
      <div className={styles.contentContainer}>
        <div className={styles.imageWrapper}>
          <img className={styles.image}  src={productImg} alt={name}/>
        </div>
        <p> {name}, {color} ({productCode}) </p>
        <div className={styles.infoWrapper}>
          <span>{price}$</span>
          <button onClick={(event) => {
            handleDelete()
            event.target.blur();
          }} >Delete from cart</button>
        </div>
        <div className={styles.itemCount}>{count}</div>
      </div>
  )
}

export default CartItem;
