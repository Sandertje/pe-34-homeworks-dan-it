import React, {useEffect, useState} from 'react';
// import styles from './cart.module.scss';
import CartItem from "../CartItem/CartItem";
import styles from "../ItemsContainer/itemsContainer.module.scss";
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import {getProductsData, getProductsDataFromLS} from "../../store/actionsCreator/productsCreator";
import {getCartItemsFromLS} from "../../store/actionsCreator/cartCreator";



function Cart(){
  const [cartAmount, setCartAmount] = useState(0);
  const {cartItems} = useSelector(state => state.cart, shallowEqual);
  const dispatch = useDispatch();

    useEffect(()=> {
      let sum = 0;
      cartItems.forEach(element => sum += element.price * element.count);
      setCartAmount(sum);
    }, [cartItems]);

    useEffect(() => {
      const lSCart = JSON.parse(localStorage.getItem("cart"));
      console.log(lSCart);
      if (lSCart) {
        dispatch(getCartItemsFromLS(lSCart));
      }
    }, []);

  return (

    <section className={styles.container}>
      <div className={styles.cartAmount}>Total price:<span>{cartAmount}$</span></div>
      <div className={styles.containerContent}>
        {cartItems.length  > 0 && cartItems.map(item =>
          <CartItem
            key={item.productImg}
            {...item}
          />)}
      </div>

    </section>
    )
}

export default Cart;

//
// <section>
//   <h2>CART</h2>
//   <div>
//     {cart && cart.map(item => <CartItem key={item.name} {...item} actionWithModal={actionWithModal} />)}
//   </div>
//   <p>Price:{cartAmount}</p>
// </section>
