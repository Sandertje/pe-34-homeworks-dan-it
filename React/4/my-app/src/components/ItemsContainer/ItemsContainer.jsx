import styles from './itemsContainer.module.scss';
import Item from "../Item/Item";
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {getProductsData, getProductsDataFromLS} from "../../store/actionsCreator/productsCreator";


function ItemsContainer () {
  const {posts} = useSelector(state => state.products, shallowEqual);
  const dispatch = useDispatch();

  useEffect( () => {
    const lSFavourite = JSON.parse(localStorage.getItem("favorites"));
    if (lSFavourite) {
      dispatch(getProductsDataFromLS(lSFavourite))
      return;
    }
    dispatch(getProductsData())
  }, []);

  return (
      <section className={styles.container}>
        <div className={styles.containerContent}>
          {posts.length > 0 && posts.map(item =>
            <Item key={item.id}
                  {...item}
                  />
            )
          }
        </div>
      </section>
    );
}

// ItemsContainer.propTypes = {
//   content: PropTypes.array.isRequired,
//   toggleFav: PropTypes.func.isRequired,
//   changingModalState: PropTypes.func.isRequired,
// }
//
// ItemsContainer.defaultProps = {
//   content: [],
//   toggleFav: ()=>{},
//   changingModalState: ()=>{},
// }


export default ItemsContainer;
