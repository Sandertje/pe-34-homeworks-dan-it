import {Switch, Route} from 'react-router-dom';
import ProductsPages from "../pages/ProductsPage/ProductsPages";
import CartPages from "../pages/CartPages/CartPages";
import FavouritePages from "../pages/FavouritePages/FavouritePages";


function Routes() {
  return (
    <Switch>
      <Route exact path="/products">
        <ProductsPages />
      </Route>
      <Route exact path="/cart">
        <CartPages />
      </Route>
      <Route exact path="/favourite">
        <FavouritePages />
      </Route>
    </Switch>
  )
}


export default Routes
