import React from "react";
import styles from './App.module.scss';
import Button from "./components/button/Button";
import Modal from "./components/modal/Modal";

class App extends React.Component {
  state = {
    isOpenFirst: false,
    isOpenSecond: false,
  }

  switchModalState = (numberOfModal) => {
    if (numberOfModal === 1) {
      this.setState(current => ({...current, isOpenFirst: !current.isOpenFirst}));
    } else {
      this.setState(current => ({...current, isOpenSecond: !current.isOpenSecond}));
    }
  }

  render() {
    return (
      <div className={styles.App}>
        <Button
          backgroundColor="blue"
          text={"Open first modal"}
          onClick={(event) => {
            this.switchModalState(1)
            event.target.blur();
          }}
        />

        {this.state.isOpenFirst && <Modal
          header="Do you want to delete this file?"
          closeButton={ () => this.switchModalState(1)}
          text="If you delete this file, you have no way to get it back
           Are you sure you want to delete it?"
          actions={
            <>
              <Button
                onClick={ () => this.switchModalState(1)}
                text={"Ok"}
                backgroundColor={"yellow"}
              />
              <Button
                onClick={ () => this.switchModalState(1)}
                text={"Cancel"}
                backgroundColor={"blue"}
              />
            </>
          }
        />}

        <Button
          backgroundColor={"yellow"}
          text={"Open second modal"}
          onClick={(event) => {
            this.switchModalState(2)
            event.target.blur();
          }}
        />

        {this.state.isOpenSecond && <Modal
          header="Beginning the test of the first paper on React"
          closeButton={ () => this.switchModalState(2)}
          text="This modal window is designed to test React's work. Do you want to close the window?"
          actions={<>
            <Button
              onClick={ () => this.switchModalState(2)}
              text={"Bugaga"}
              backgroundColor={"pink"}/>
            <Button
              onClick={() => this.switchModalState(2)}
              text ={"Leave"}
              backgroundColor={"green"}/>
          </>}
        />}
      </div>
    );
  }
}

export default App;
