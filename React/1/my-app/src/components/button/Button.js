import React from 'react';
import styles from './button.module.scss';

class Button extends React.PureComponent {
  render(){
    const {backgroundColor, text, onClick} = this.props;

    return (
      <button className={styles.button} onClick={onClick} style={{backgroundColor}}>{text}</button>
    );
  }
}


export default Button;
