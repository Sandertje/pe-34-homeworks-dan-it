import React from 'react';
import styles from './modal.module.scss';

class Modal extends React.PureComponent {
  render(){
    const {header, closeButton, text, actions} = this.props;

    return (
      <div className={styles.modal}>
        <div className={styles.container}>
          <div className={styles.closeButtonWrapper}>
            <button onClick={closeButton}>x</button>
          </div>
          <div className={styles.contentWrapper}>
            <div className={styles.contentWrapperTitle}>{header}</div>
            <div className={styles.contentWrapperText}>{text}</div>
          </div>
          <div>
            {actions}
          </div>
        </div>
        <div onClick={closeButton} className={styles.background}/>
      </div>

    );
  }
}


export default Modal;
