import styles from './cartItem.module.scss';
import {useDispatch} from "react-redux";
import {setIsModalOpen, setModalParams} from "../../store/actionsCreator/modalCreator";
import {makeLessItem, makeMoreItem} from "../../store/actionsCreator/cartCreator";
import { ReactComponent as DeletePic } from "../../assets/svg/delete.svg"
import { ReactComponent as Remove } from "../../assets/svg/count_minus.svg"
import { ReactComponent as Add } from "../../assets/svg/count_plus.svg"

const CartItem = ({name, price, productCode, color, productImg, count, id}) => {

  const dispatch = useDispatch();

  const handleDelete = () => {
    dispatch(setModalParams({
      isHasAddAction: false,
      id,
      modalTitle: `Do you really want to delete ${name} from your cart?`,
      name,
      })
    )
    dispatch(setIsModalOpen(true));
  }

  return (
    <div className={styles.container}>
      <div className={styles.productMainInfo}>
        <img className={styles.productMainInfoImg} src={productImg} alt={name}/>
        <p> {name}, {color} ({productCode}) </p>
        <div onClick={(event) => {
          handleDelete()
          event.target.blur();
        }} > <DeletePic/> </div>
      </div>
      <div className={styles.countContainer}>
        <div className={styles.countPart}>
          <div onClick={ () => {
            dispatch(makeLessItem(id))
          }}> <Remove/> </div>
          <div> {count} </div>
          <div onClick={ () => {
            dispatch(makeMoreItem(id))
          }}> <Add/> </div>
        </div>
        <span>{price * count}$</span>
      </div>
    </div>
  )
}

export default CartItem;
