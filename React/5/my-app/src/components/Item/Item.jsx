import styles from './item.module.scss';
import { ReactComponent as StarAdd } from "../../assets/svg/plus.svg";
import { ReactComponent as StarRemove } from "../../assets/svg/remove.svg";
import {useDispatch} from "react-redux";
import {changeFavouriteState} from "../../store/actionsCreator/productsCreator";
import {setModalParams, setIsModalOpen} from "../../store/actionsCreator/modalCreator";


function Item({name, price, productCode, color, productImg, id, isFavourite}) {
  const dispatch = useDispatch();

  const handleAdd = () => {
    dispatch(setModalParams({
      isHasAddAction: true,
      id,
      modalTitle: `Do you really want to add ${name} to your cart?`,
      name,
      price,
      productCode,
      color,
      productImg,
      })
    )

    dispatch(setIsModalOpen(true));
  }

  return (
      <div className={styles.container}>
        <div className={styles.favorite} onClick={ () => {
          dispatch(changeFavouriteState(id))
        }} >
          {isFavourite && <StarRemove/>}
          {!isFavourite && <StarAdd />}
        </div>
        <div className={styles.imageWrapper}>
          <img className={styles.image}  src={productImg} alt={name}/>
        </div>
        <p> {name}, {color} ({productCode}) </p>
        <div className={styles.infoWrapper}>
          <span>{price}$</span>
          <button onClick={(event) => {
            handleAdd();
            event.target.blur();
          }} >Add to cart</button>
        </div>
      </div>
  );
}

// Item.PropTypes = {
//   name: PropTypes.string.isRequired,
//   price: PropTypes.number.isRequired,
//   productCode: PropTypes.number.isRequired,
//   color: PropTypes.string.isRequired,
//   productImg: PropTypes.string.isRequired,
//   id: PropTypes.number.isRequired,
//   isFavourite: PropTypes.bool.isRequired,
//   toggleFav: PropTypes.func.isRequired,
//   changingModalState: PropTypes.func.isRequired,
// }
//
// Item.defaultProps = {
//   name: "",
//   price: null,
//   productCode: null,
//   color: "",
//   productImg: "" ,
//   isFavourite: false,
//   toggleFav: ()=>{},
//   changingModalState: ()=>{},
// }

export default Item;
