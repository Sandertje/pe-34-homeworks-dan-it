import {useEffect, useState} from 'react';
import styles from './cart.module.scss';
import CartItem from "../CartItem/CartItem";
import {shallowEqual, useDispatch, useSelector} from "react-redux";
import {getCartItemsFromLS} from "../../store/actionsCreator/cartCreator";
import {Button} from "@mui/material";
import {Formik, Form} from "formik";
import FormInput from "../FormInput/FormInput";
import * as yup from 'yup';
import {cleanCart} from "../../store/actionsCreator/cartCreator";


function Cart(){
  const [cartAmount, setCartAmount] = useState(0);
  const {cartItems} = useSelector(state => state.cart, shallowEqual);
  const dispatch = useDispatch();

    useEffect(()=> {
      let sum = 0;
      cartItems.forEach(element => sum += element.price * element.count);
      setCartAmount(sum);
    }, [cartItems]);

    useEffect(() => {
      const lSCart = JSON.parse(localStorage.getItem("cart"));
      if (lSCart) {
        dispatch(getCartItemsFromLS(lSCart));
      }
    }, []);

    const initialValues = {
      name: "",
      surname: "",
      age: "",
      address: "",
      telephone: "",
    }

    const handleSubmit = (value, {resetForm}) => {
      console.log(value);
      console.log(cartItems);
      dispatch(cleanCart());
      localStorage.setItem("cart", JSON.stringify([]));
      resetForm();
    }

  const validationSchema = yup.object().shape({
    name: yup.string()
      .required("Field is required")
      .min(3, "Must be more then 3 symbols")
      .max(24, "Must be no more then 24 symbols"),
    surname:  yup.string()
      .required("Field is required")
      .min(3, "Must be more then 3 symbols")
      .max(24, "Must be no more then 24 symbols"),
    age:  yup.string()
      .required("Field is required")
      .min(1, "Must be more then 1 symbols")
      .max(3, "Must be no more then 3 symbols")
      .matches(/[0-9]/, "Only numbers"),
    address:  yup.string()
      .required("Field is required")
      .min(3, "Must be more then 3 symbols")
      .max(24, "Must be no more then 24 symbols"),
    telephone:  yup.string()
      .required("Field is required")
      .min(6, "Must be more then 6 symbols")
      .max(24, "Must be no more then 24 symbols")
      .matches(/^(\+)?((\d{2,3}) ?\d|\d)(([ -]?\d)|( ?(\d{2,3}) ?)){5,12}\d$/, "Incorrect telephone format"),
  });

  return (
    <section className={styles.mainContainer}>
      <div className={styles.formContainer}>
        <h2>Please, fill the form for delivery</h2>
        <Formik initialValues={initialValues} onSubmit={handleSubmit} validationSchema={validationSchema}>
          <Form className={styles.form}>
            <FormInput name="name" label="name" type="text"/>
            <FormInput name="surname" label="surname" type="text"/>
            <FormInput name="age" label="age" type="text"/>
            <FormInput name="address" label="address" type="text"/>
            <FormInput name="telephone" label="telephone" type="tel"/>
            <Button type="submit" variant="contained">Checkout</Button>
          </Form>
        </Formik>
        <span className={styles.totalPrice}>Total price: {cartAmount}$</span>
      </div>
      <div className={styles.itemsContainer}>
        {cartItems.length  > 0 && cartItems.map(item =>
          <CartItem
            key={item.productImg}
            {...item}
          />)}
      </div>
    </section>
    )
}

export default Cart;
