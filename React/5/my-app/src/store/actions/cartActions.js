export const ADD_ITEM = 'ADD_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const GET_CART_ITEM_FROM_LS = 'GET_CART_ITEM_FROM_LS';
export const CLEAN_CART = 'CLEAN_CART';
export const MAKE_LESS_ITEMS = 'MAKE_LESS_ITEMS';
export const MAKE_MORE_ITEMS = 'MAKE_MORE_ITEMS';

