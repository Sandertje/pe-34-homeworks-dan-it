import {SET_IS_MODAL_OPEN, SET_MODAL_PARAMS} from "../actions/modalActions";

const initialValues = {
  isOpen: false,
  isHasAddAction: true,
  id: null,
  title: null,
  modalTitle: "",
  price: null,
  productCode: null,
  color: null,
  productImg: null,
}

const modalReducer = (state = initialValues, action) => {
  switch (action.type){
    case SET_IS_MODAL_OPEN:
      return {...state, isOpen: action.payload};
    case SET_MODAL_PARAMS:
      return {...state,
        isHasAddAction: action.payload?.isHasAddAction,
        id: action.payload?.id,
        modalTitle: action.payload?.modalTitle,
      };
    default:
      return state;
  }

}

export default modalReducer;
