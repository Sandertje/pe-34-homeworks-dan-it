import {combineReducers} from "redux";
import {productsReducer} from "./productsReducer";
import cartReducer from "./cartReducer";
import modalReducer from "./modalReducer";

const reducer = combineReducers({
  products: productsReducer,
  cart: cartReducer,
  modal: modalReducer,
});

export default reducer;
