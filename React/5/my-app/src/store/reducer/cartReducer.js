import {ADD_ITEM, REMOVE_ITEM, GET_CART_ITEM_FROM_LS, CLEAN_CART, MAKE_LESS_ITEMS, MAKE_MORE_ITEMS} from "../actions/cartActions"

const initialValues = {
  cartItems: [],
}

const cartReducer = (state = initialValues, action) => {
  switch (action.type) {
    case ADD_ITEM: {
      const newCartItems = [...state.cartItems];
      const index = newCartItems.findIndex(elem => elem.id === action.payload.id);

      if (index === -1) {
        const newItem = {...action.payload, count: 1};
        localStorage.setItem("cart", JSON.stringify([...state.cartItems, newItem]));
        return {...state, cartItems: [...state.cartItems, newItem]};
      }

      newCartItems[index].count = newCartItems[index].count + 1;
      localStorage.setItem("cart", JSON.stringify(newCartItems));
      return {...state, cartItems: newCartItems};
    }
    case REMOVE_ITEM: {
      const newCartItems = [...state.cartItems];
      const index = newCartItems.findIndex(elem => elem.id === action.payload);

      if (index === -1) {
        return {...state};
      }

      newCartItems.splice(index, 1);
      localStorage.setItem("cart", JSON.stringify(newCartItems));
      return {...state, cartItems: newCartItems}
    }
    case GET_CART_ITEM_FROM_LS:

      return {...state, cartItems: action.payload};

    case CLEAN_CART:

      return {...state, cartItems: []}

    case MAKE_LESS_ITEMS: {
      const newCartItems = [...state.cartItems];
      const index = newCartItems.findIndex(elem => elem.id === action.payload);
      if (newCartItems[index].count === 1) {
        return {...state, cartItems: newCartItems};
      }
      newCartItems[index].count = newCartItems[index].count - 1;
      return {...state, cartItems: newCartItems};
    }

    case MAKE_MORE_ITEMS: {
      const newCartItems = [...state.cartItems];
      const index = newCartItems.findIndex(elem => elem.id === action.payload);
      newCartItems[index].count = newCartItems[index].count + 1;
      return { ...state, cartItems: newCartItems}
    }

    default:
      return state;
  }

}

export default cartReducer;
