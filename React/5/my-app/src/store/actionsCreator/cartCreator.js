import {ADD_ITEM, REMOVE_ITEM, GET_CART_ITEM_FROM_LS, CLEAN_CART, MAKE_LESS_ITEMS, MAKE_MORE_ITEMS} from "../actions/cartActions";

export const addItemToTheCart = (cartItem) => ({
  type: ADD_ITEM,
  payload: cartItem,
});

export const removeItemFromTheCart = (id) => ({
  type: REMOVE_ITEM,
  payload: id,
});

export const getCartItemsFromLS = (item) => ({
  type: GET_CART_ITEM_FROM_LS,
  payload: item,
});

export const cleanCart = () => ({
  type: CLEAN_CART,
});

export const makeLessItem = (id) => ({
  type: MAKE_LESS_ITEMS,
  payload: id,
});

export const makeMoreItem = (id) => ({
  type: MAKE_MORE_ITEMS,
  payload: id,
});
