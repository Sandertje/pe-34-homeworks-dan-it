import React from "react";
import styles from './App.module.scss';
import ItemsContainer from "./components/ItemsContainer/ItemsContainer";
import Modal from "./components/Modal/Modal";




class App extends React.Component {

  state = {
    content: [],
    isModalOpen: false,
    currentProductId: null,
  }

  changingModalState = (id) => {
    if (id) {
      this.setState(current => ({...current, isModalOpen: !current.isModalOpen, currentProductId: id}));
    } else {
      this.setState(current => ({...current, isModalOpen: !current.isModalOpen, currentProductId: null}));
    }
  }

  toggleAndAddFav = (name, id) => {
    const index = this.state.content.findIndex(({name: arrayName}) => {
      return name === arrayName;
    });
    const content = [...this.state.content];
    content[index].isFavourite = !content[index].isFavourite;
    this.setState({content});

    const favourite = JSON.parse(localStorage.getItem("favorites"));
    const filteredFavourite = favourite.filter((element) => {
      return element.id !== id;
    });
    filteredFavourite.push(content[index]);
    localStorage.setItem("favorites", JSON.stringify(filteredFavourite));
    }

  addProductToCart = (id) => {
    const cart = JSON.parse(localStorage.getItem("cart"));
    const product = this.state.content.find(element => element.id === id);
    cart.push(product);
    localStorage.setItem("cart", JSON.stringify(cart));
    this.setState(current => ({...current, isModalOpen: !current.isModalOpen, currentProductId: null}));
  }

  async componentDidMount() {
    if (!localStorage.getItem("cart") || !localStorage.getItem("favorites")) {
      localStorage.setItem("cart", JSON.stringify([]));
      localStorage.setItem("favorites", JSON.stringify([]));
    }

    const response = await fetch('./ItemsArray.json')
      .then(e => e.json());
    this.setState(current => ({...current, content: response}));
  }

  render() {
    const { content, isModalOpen, currentProductId } = this.state;
    return (
      <div className={styles.App}>
        <ItemsContainer
            content={content}
            toggleAndAddFav={this.toggleAndAddFav}
            changingModalState={this.changingModalState}
        />
        {isModalOpen && <Modal
          header="Are you sure you want to add this product to your cart?"
          closeModal={this.changingModalState}
          onConfirm={(id) => this.addProductToCart(id)}
          currentProductId={currentProductId}
        />
        }
      </div>
    );
  }
}

export default App;
