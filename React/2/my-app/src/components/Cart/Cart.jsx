import React from 'react';
import styles from './cart.module.scss';
import {ReactComponent as StarRemove} from "../../assets/svg/star-remove.svg";
import {ReactComponent as StarAdd} from "../../assets/svg/star-plus.svg";
import CartItem from "../CartItem/CartItem";


class Cart extends React.PureComponent {
  render(){
    const {items} = this.props;
    return (
      <section>
        <h2>CART</h2>
        <div>
          {items && items.map(item => <CartItem {...item} />)}
        </div>
        <p>Price: </p>
      </section>
    )
  }
}

export default Cart;
