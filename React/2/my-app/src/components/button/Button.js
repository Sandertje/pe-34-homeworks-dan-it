import React from 'react';
import styles from './button.module.scss';
import PropTypes from "prop-types";

class Button extends React.PureComponent {
  render(){
    const {text, onClick} = this.props;

    return (
      <button onClick={onClick}> {text} </button>
    );
  }
}

Button.propTypes = {
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
}

Button.defaultProps = {
  text: "",
  onClick: () => {},
}



export default Button;
