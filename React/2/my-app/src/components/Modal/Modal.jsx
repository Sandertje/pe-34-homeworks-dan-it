import React from 'react';
import styles from './modal.module.scss';
import Button from "../button/Button";

class Modal extends React.Component {
  render(){
    const {header, closeModal, onConfirm, currentProductId} = this.props;

    return (
      <div className={styles.modal}>
        <div className={styles.container}>
          <div className={styles.closeButtonWrapper}>
            <button onClick={closeModal}>x</button>
          </div>
          <div className={styles.contentWrapper}>
            <p className={styles.contentWrapperTitle}>{header}</p>
          </div>
          <div className={styles.buttonWrapper}>
             <Button onClick={()=>{onConfirm(currentProductId)}} text={"Ok"}/>
             <Button onClick={closeModal} text={"Cancel"}/>
          </div>
        </div>
        <div onClick={closeModal} className={styles.background}/>
      </div>
    );
  }
}

export default Modal;
