import React from 'react';
import styles from './item.module.scss';
import { ReactComponent as StarAdd } from "../../assets/svg/star-plus.svg";
import { ReactComponent as StarRemove } from "../../assets/svg/star-remove.svg";
import PropTypes from "prop-types";


class Item extends React.PureComponent {
  render(){
    const {name, price, productCode,
      color, productImg, id,
      isFavourite, toggleAndAddFav,
      changingModalState} = this.props;

    return (
      <div className={styles.container}>
        <div className={styles.favorite} onClick={() => toggleAndAddFav(name, id)} >
          {isFavourite && <StarRemove/>}
          {!isFavourite && <StarAdd />}
        </div>
        <div className={styles.imageWrapper}>
          <img className={styles.image}  src={productImg} alt={name}/>
        </div>
        <p> {name}, {color} ({productCode}) </p>
        <div className={styles.infoWrapper}>
          <span>{price}$</span>
          <button onClick={(event) => {
            changingModalState(id);
            event.target.blur();
          }}>Add to cart</button>
        </div>
      </div>
    );
  }
}

Item.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  productCode: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired,
  productImg: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  isFavourite: PropTypes.bool.isRequired,
  toggleFav: PropTypes.func.isRequired,
  changingModalState: PropTypes.func.isRequired,
}

Item.defaultProps = {
  name: "",
  price: null,
  productCode: null,
  color: "",
  productImg: "" ,
  isFavourite: false,
  toggleFav: ()=>{},
  changingModalState: ()=>{},
}

export default Item;
