import React from 'react';
import styles from './itemsContainer.module.scss';
import Item from "../Item/Item";
import PropTypes from "prop-types";

class ItemsContainer extends React.PureComponent {
  render() {
    const {content, toggleAndAddFav, changingModalState} = this.props;
    return (
      <section className={styles.container}>
        <h1>Wireless headphones</h1>
        <div className={styles.containerContent}>
          {content && content.map(item =>
            <Item key={item.id}
                  {...item}
                  toggleAndAddFav={toggleAndAddFav}
                  changingModalState={changingModalState}
                  />
            )
          }
        </div>
      </section>
    );
  }
}

ItemsContainer.propTypes = {
  content: PropTypes.array.isRequired,
  toggleFav: PropTypes.func.isRequired,
  changingModalState: PropTypes.func.isRequired,
}

ItemsContainer.defaultProps = {
  content: [],
  toggleFav: ()=>{},
  changingModalState: ()=>{},
}


export default ItemsContainer;
