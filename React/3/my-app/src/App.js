import Header from "./components/Header/Header";
import {useState, useEffect} from "react";
import { BrowserRouter } from "react-router-dom";
import Routes from "./routes/Routes";
import Modal from "./components/Modal/Modal";
import './App.module.scss';



function App() {
  const [posts, setPosts] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isAddModal, setIsAddModal] = useState(true);
  const [currentProductInfo, setCurrentProductInfo] = useState({});
  const [cart, setCart] = useState([]);


  useEffect(() => {

    if (!localStorage.getItem("cart") || !localStorage.getItem("favorites")) {
      localStorage.setItem("cart", JSON.stringify([]));
      localStorage.setItem("favorites", JSON.stringify([]));
    }

    (async ()=>{
        const response = await fetch('./Posts.json')
          .then(response => response.json());

        const lSFavourite = JSON.parse(localStorage.getItem("favorites"));
        const lSCart = JSON.parse(localStorage.getItem("cart"));

        console.log("lSCart", lSCart)
        console.log("lSFavourite", lSFavourite)

          lSFavourite.forEach(element => {
            const currentPostsIndex = response.findIndex(({id}) => element.id === id);
            response[currentPostsIndex].isFavourite = true;
          })

        setPosts(response);
        setCart(lSCart);
    }
    )()
  },[]);


  const changingModalState = () => {
    setIsModalOpen(current => {
      return !current;
    })
  }

  const actionWithModal = (type, name) => {
    const currentPostsIndex = posts.findIndex(({name: arrayName}) => arrayName === name );
    if (type === "delete") {
      setIsAddModal(false);
      setCurrentProductInfo({name});
    } else {
      setIsAddModal(true);
      setCurrentProductInfo(posts[currentPostsIndex]);
    }
    changingModalState();
  }

  const addProductToCart = (currentProductInfo) => {
    const index = cart.findIndex(({name}) => name === currentProductInfo.name);
    addProductToCartLocalStorage(currentProductInfo);
    if (index === -1) {
      currentProductInfo.count = 1;
      setCart(current => [...current, currentProductInfo]);
    } else {
      setCart(current => {
        const newState = [...current];
        newState[index].count = current[index].count + 1;
        return newState;
      });
    }

    changingModalState();
  }

  const deleteProductFromCart = ({name}) => {
    const index = cart.findIndex(({name: arrayName}) => arrayName === name );
    setCart(current => {
      const newState = [...current];
      if (newState[index].count === 1) {
        newState.splice(index, 1);
        changeCartLocalStorageState(newState);
        return newState;
      } else {
        newState[index].count = newState[index].count - 1;
        changeCartLocalStorageState(newState);
        return newState;
      }
    })

    changingModalState();
  }

  const addProductToCartLocalStorage = (currentProductInfo) => {
    const localStCart = JSON.parse(localStorage.getItem("cart"))
    const lSIndex = localStCart.findIndex(({name}) => name === currentProductInfo.name);

    if (lSIndex === -1) {
      currentProductInfo.count = 1;
      localStCart.push(currentProductInfo);
      localStorage.setItem("cart", JSON.stringify(localStCart));
    } else {
      localStCart[lSIndex].count = localStCart[lSIndex].count + 1;
      localStorage.setItem("cart", JSON.stringify(localStCart));
    }
  }

  const toggleFav = (name) => {

      const currentPostsIndex = posts.findIndex(({name: arrayName}) => arrayName === name);

      setPosts(current => {
        const newState = [...current];
        newState[currentPostsIndex].isFavourite = !newState[currentPostsIndex].isFavourite;
        const filteredPosts = newState.filter(element => element.isFavourite === true);
        changeFavouriteLocalStorageState(filteredPosts);
        return newState;
      })
  }

  const changeCartLocalStorageState = (item) => {
    localStorage.setItem("cart", JSON.stringify(item));
  }

  const changeFavouriteLocalStorageState = (item) => {
    localStorage.setItem("favorites", JSON.stringify(item));
  }

  return (
    <BrowserRouter>
      <div className="App">
        <Header/>
        <Routes posts={posts}
                actionWithModal={actionWithModal}
                cart={cart}
                toggleFav={toggleFav}
        />
        <Modal isModalOpen={isModalOpen}
               changingModalState={changingModalState}
               modalAction={ () => {
                     if (isAddModal) {
                       addProductToCart(currentProductInfo);
                     } else {
                       deleteProductFromCart(currentProductInfo)
                   }
                 }
               }
               title={
                 isAddModal ?
                   `Are you sure you want to add ${currentProductInfo.name} to your cart?` :
                   `Are you sure you want to delete ${currentProductInfo.name} from the cart? `
               }
        />
      </div>
    </BrowserRouter>
  );
}

export default App;
