import {Switch, Route, Redirect} from 'react-router-dom';
import ProductsPages from "../pages/ProductsPage/ProductsPages";
import CartPages from "../pages/CartPages/CartPages";
import FavouritePages from "../pages/FavouritePages/FavouritePages";


function Routes({posts, actionWithModal, cart, toggleFav}) {
  return (
    <Switch>
      <Route exact path="/">
        <Redirect to="/products"/>
      </Route>
      <Route exact path="/products">
        <ProductsPages posts={posts} actionWithModal={actionWithModal} toggleFav={toggleFav}/>
      </Route>
      <Route exact path="/cart">
        <CartPages cart={cart} actionWithModal={actionWithModal}/>
      </Route>
      <Route exact path="/favourite">
        <FavouritePages actionWithModal={actionWithModal} posts={posts} toggleFav={toggleFav}/>
      </Route>
    </Switch>
  )
}


export default Routes
