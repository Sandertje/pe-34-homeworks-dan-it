import React, {useEffect, useState} from 'react';
import CartItem from "../CartItem/CartItem";
import styles from "../ItemsContainer/itemsContainer.module.scss";
import PropTypes from "prop-types";


function Cart({cart, actionWithModal}){
    const [cartAmount, setCartAmount] = useState(0);

    useEffect(()=> {
      let sum = 0;
      cart.forEach(e => sum += e.price * e.count);
      setCartAmount(sum);
    }, [cart]);

  return (
    <section className={styles.container}>
      <div className={styles.cartAmount}>Total price:<span>{cartAmount}$</span></div>
      <div className={styles.containerContent}>
        {cart && cart.map(item =>
          <CartItem
            key={item.productImg}
            {...item}
            actionWithModal={actionWithModal}
          />)}
      </div>
    </section>
    )
}

Cart.propTypes = {
  cart: PropTypes.array.isRequired,
  actionWithModal: PropTypes.func.isRequired,
}

Cart.defaultProps = {
  cart: [],
  actionWithModal: () => {},
}

export default Cart;
