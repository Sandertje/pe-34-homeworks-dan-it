import React from "react";
import styles from "./header.module.scss"
import {NavLink} from "react-router-dom";


const Header = () => {
  return(
    <nav>
      <ul className={styles.menu}>
        <li className={styles.menuItem}>
          <NavLink to="/products" className={styles.menuItemLink} activeClassName={styles.activeLink}>Products</NavLink>
        </li>
        <li className={styles.menuItem}>
          <NavLink exact to="/cart" className={styles.menuItemLink} activeClassName={styles.activeLink}>Cart</NavLink>
        </li>
        <li className={styles.menuItem}>
          <NavLink to="/favourite" className={styles.menuItemLink} activeClassName={styles.activeLink}>Favourite</NavLink>
        </li>
      </ul>
    </nav>
  )
}

export default Header
