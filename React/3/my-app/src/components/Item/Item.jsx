import styles from './item.module.scss';
import { ReactComponent as StarAdd } from "../../assets/svg/plus.svg";
import { ReactComponent as StarRemove } from "../../assets/svg/remove.svg";
import PropTypes from "prop-types";
import Button from "../Button/Button";

function Item({name, price, productCode, color, productImg, isFavourite, actionWithModal, toggleFav}) {

  return (
      <div className={styles.container}>
        <div className={styles.favorite} onClick={ () => {
          toggleFav(name, "posts")
        }} >
          {isFavourite && <StarRemove/>}
          {!isFavourite && <StarAdd />}
        </div>
        <div className={styles.imageWrapper}>
          <img className={styles.image}  src={productImg} alt={name}/>
        </div>
        <p> {name}, {color} ({productCode}) </p>
        <div className={styles.infoWrapper}>
          <span>{price}$</span>
          <Button
            onClick={(event) => {
            actionWithModal("add", name)
            event.target.blur();
          }}
            text={"Add to cart"}
          />
        </div>
      </div>
  );
}


Item.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  productCode: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired,
  productImg: PropTypes.string.isRequired,
  isFavourite: PropTypes.bool.isRequired,
  toggleFav: PropTypes.func.isRequired,
  changingModalState: PropTypes.func.isRequired,
}

Item.defaultProps = {
  name: "",
  price: null,
  productCode: null,
  color: "",
  productImg: "",
  isFavourite: false,
  toggleFav: () => {},
  changingModalState: () => {},
}

export default Item;
