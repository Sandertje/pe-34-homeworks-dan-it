import styles from "./favouriteItem.module.scss"
import { ReactComponent as StarAdd } from "../../assets/svg/plus.svg";
import { ReactComponent as StarRemove } from "../../assets/svg/remove.svg";
import PropTypes from "prop-types";


function FavouriteItem({name, price, productCode, color, productImg, isFavourite, actionWithModal, toggleFav}) {

  return (
    <div className={styles.container}>
      <div className={styles.favorite} onClick={ () => {
        toggleFav(name)
      }}  >
        {isFavourite && <StarRemove/>}
        {!isFavourite && <StarAdd />}
      </div>
      <div className={styles.imageWrapper}>
        <img className={styles.image}  src={productImg} alt={name}/>
      </div>
      <p> {name}, {color} ({productCode}) </p>
      <div className={styles.infoWrapper}>
        <span>{price}$</span>
        <button onClick={(event) => {
          actionWithModal("add", name)
          event.target.blur();
        }} >Add to cart</button>
      </div>
    </div>
  );
}

FavouriteItem.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  productCode: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired,
  productImg: PropTypes.string.isRequired,
  isFavourite: PropTypes.bool.isRequired,
  actionWithModal: PropTypes.func.isRequired,
  toggleFav: PropTypes.func.isRequired,
}

FavouriteItem.defaultProps = {
  name: "",
  price: null,
  productCode: null,
  color: "",
  productImg: "" ,
  isFavourite: null,
  actionWithModal: () => {},
  toggleFav: () => {},
}

export default FavouriteItem;
