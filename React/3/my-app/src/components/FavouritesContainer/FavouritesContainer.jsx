import styles from "./favouritesContainer.module.scss"
import FavouriteItem from "../FavouriteItem/FafouriteItem";
import PropTypes from "prop-types";

function FavouriteContainer ({posts, actionWithModal, toggleFav}) {

  return (
    <section className={styles.container}>
      <div className={styles.containerContent}>
        {posts && posts.map(item =>
          item.isFavourite &&
          <FavouriteItem key={item.productCode}
                         {...item}
                         toggleFav={toggleFav}
                         actionWithModal={actionWithModal}
          />
        )
        }
      </div>
    </section>
  );
}

FavouriteContainer.propTypes = {
  posts: PropTypes.array.isRequired,
  actionWithModal: PropTypes.func.isRequired,
  toggleFav: PropTypes.func.isRequired,
}

FavouriteContainer.defaultProps = {
  posts: [],
  actionWithModal: () => {},
  toggleFav: () => {},
}

export default FavouriteContainer;
