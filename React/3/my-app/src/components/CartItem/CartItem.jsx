import styles from './cartItem.module.scss';
import PropTypes from "prop-types";
import Button from "../Button/Button";

const CartItem = ({name, price, productCode, color, productImg, count, actionWithModal}) => {

  return (

      <div className={styles.contentContainer}>
        <div className={styles.imageWrapper}>
          <img className={styles.image}  src={productImg} alt={name}/>
        </div>
        <p> {name}, {color} ({productCode}) </p>
        <div className={styles.infoWrapper}>
          <span> {price}$ </span>
          <Button
            onClick={(event) => {
            actionWithModal("delete", name)
            event.target.blur();
          }}
            text={"Delete from cart"}
          />
        </div>
        <div className={styles.itemCount}>{count}</div>
      </div>

  )
}

CartItem.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  productCode: PropTypes.number.isRequired,
  color: PropTypes.string.isRequired,
  productImg: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  actionWithModal: PropTypes.func.isRequired,
}

CartItem.defaultProps = {
  name: "",
  price: null,
  productCode: null,
  color: "",
  productImg: "" ,
  count: null,
  actionWithModal: ()=>{},
}

export default CartItem;
