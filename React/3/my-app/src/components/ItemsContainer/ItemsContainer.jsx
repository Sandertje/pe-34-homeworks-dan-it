import styles from './itemsContainer.module.scss';
import Item from "../Item/Item";
import PropTypes from "prop-types";

function ItemsContainer ({posts, actionWithModal, toggleFav}) {

  return (
      <section className={styles.container}>
        <div className={styles.containerContent}>
          {posts && posts.map(item =>
            <Item key={item.id}
                  {...item}
                  toggleFav={toggleFav}
                  actionWithModal={actionWithModal}
                  />
            )
          }
        </div>
      </section>
    );
}

ItemsContainer.propTypes = {
  content: PropTypes.array.isRequired,
  toggleFav: PropTypes.func.isRequired,
  changingModalState: PropTypes.func.isRequired,
}

ItemsContainer.defaultProps = {
  content: [],
  toggleFav: () => {},
  changingModalState: () => {},
}


export default ItemsContainer;
