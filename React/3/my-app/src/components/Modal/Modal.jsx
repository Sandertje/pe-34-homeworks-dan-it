import React from 'react';
import styles from './modal.module.scss';
import PropTypes from "prop-types";
import Button from "../../../../../6/my-app/src/components/Button/Button";

function Modal({isModalOpen, changingModalState, modalAction, title}) {

  if (!isModalOpen) return null;

    return (
      <div className={styles.modal}>
        <div className={styles.container}>
          <div className={styles.closeButtonWrapper}>
            <button onClick={changingModalState} >x</button>
          </div>
          <div className={styles.contentWrapper}>
            <p className={styles.contentWrapperTitle}>{title}</p>
          </div>
          <div className={styles.buttonWrapper}>
            <Button onClick={ () => {modalAction()} } text={"ok"}/>
            <Button onClick={changingModalState} text={"Cancel"}/>
          </div>
        </div>
        <div onClick={changingModalState} className={styles.background}/>
      </div>
    );

}

Modal.propTypes = {
  isModalOpen: PropTypes.bool.isRequired,
  changingModalState: PropTypes.func.isRequired,
  modalAction: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
}

Modal.defaultProps = {
  isModalOpen: null,
  changingModalState: () => {},
  modalAction: () => {},
  title: "",
}

export default Modal;


