import React from "react";
import Cart from "../../components/Cart/Cart";


const CartPages = ({cart, actionWithModal}) => {

  return (
    <>
      <Cart cart={cart} actionWithModal={actionWithModal}/>
    </>)
}

export default CartPages;
