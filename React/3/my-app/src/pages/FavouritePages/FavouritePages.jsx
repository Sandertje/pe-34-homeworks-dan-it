import FavouriteContainer from "../../components/FavouritesContainer/FavouritesContainer";

const FavouritePages = ({actionWithModal, posts, toggleFav}) => {

  return (
    <>
      <FavouriteContainer posts={posts} actionWithModal={actionWithModal} toggleFav={toggleFav}/>
    </>)
}

export default FavouritePages;
