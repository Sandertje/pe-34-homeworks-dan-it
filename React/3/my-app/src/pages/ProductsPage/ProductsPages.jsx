import ItemsContainer from "../../components/ItemsContainer/ItemsContainer";


const ProductsPages = ({posts, actionWithModal, toggleFav}) => {

  return (
    <>
      <ItemsContainer posts={posts} actionWithModal={actionWithModal} toggleFav={toggleFav}/>
    </>)
}

export default ProductsPages;
