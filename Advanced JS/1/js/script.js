class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }
  set name(value) {
    this._name = value;
  }
  get age() {
    return this._age;
  }
  set age(value) {
    this._age = value;
  }
  get salary() {
    return this._salary;
  }
  set salary(value) {
    this._salary = value;
  }
}

const Alex = new Employee("Alex", 25, 5000);
console.log(Alex);

class Programmer extends Employee {
  constructor(lang, ...args) {
    super(...args);
    this.lang = lang;
  }
  get salary() {
    return this._salary*3;
  }
}

const Jacob = new Programmer(5, "Jacob", 28, 6000);
const YourAss = new Programmer(7, "YourAss", 29, 10000);
const Timofej = new Programmer(9, "Timofej", 18, 7000);

console.log(Jacob);
console.log(YourAss);
console.log(Timofej);
