class Card {
  constructor(name, email, title, body, id) {
    this.name = name;
    this.email = email;
    this.title = title;
    this.text = body;
    this.id = id;
    this.container = document.querySelector(".container");
  }
  createCards() {
    this.container.insertAdjacentHTML("beforeend", `
      <div class="post" id="postId-${this.id}">
        <div class="post__avatar">
          <img class="post__avatar-item" src="https://i.pinimg.com/originals/a6/58/32/a65832155622ac173337874f02b218fb.png" alt=""/>
        </div>
        <div class="post__body">
          <div class="post__body-header">
            <div class="post__body-header-info">
              <h3 class="post__body-header-info-fullName">${this.name}<span class="post__header-info-mail"><span class="post__header-info-badge">verified</span>${this.email}</span></h3>
            </div>
            <div class="post__header-title">
              <p>${this.title}</p>
            </div>
          </div>
          <img src="https://www.focus2move.com/wp-content/uploads/2020/01/Tesla-Roadster-2020-1024-03.jpg" alt=""/>
          <div class="post__body-text">
            <p>${this.text}</p>
          </div>
          <div class="post__deleteButton">
            <button class="post__deleteButton-item" id="buttonId=${this.id}">Delete post</button>
          </div>
        </div>
      </div>
    `)
  }
}

Promise.all([axios.get("https://ajax.test-danit.com/api/json/users"), axios.get("https://ajax.test-danit.com/api/json/posts")]).then(res => {
  if (res[0].status === 200 && res[1].status === 200) {
    const infoAboutPerson = res[0].data;
    const postsInformation = res[1].data;
    postsInformation.forEach(item => {
      new Card (infoAboutPerson.find(elem => elem.id === item.userId).name, infoAboutPerson.find(elem => elem.id === item.userId).email.split("@")[0], item.title, item.body, item.id).createCards();
    })
    document.querySelectorAll(`.post__deleteButton-item`).forEach(element=> {
      element.addEventListener("click", event => {
        const currentPostId = event.target.id.split("=")[1];
        axios.delete(`https://ajax.test-danit.com/api/json/posts/${currentPostId}`).then(({status})=>{
          if (status === 200) {
            document.querySelector(`#postId-${currentPostId}`).remove();
          }
        }).catch(error => console.error(error));
      })
    })
  }
}).catch(error => console.error(error));



