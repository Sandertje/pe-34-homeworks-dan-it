document.querySelector(".button").addEventListener("click", async (e) => {
     try {
        const {status, data} = await axios.get("https://api.ipify.org/?format=json");
        const ipValue = data.ip;
        if (status === 200) {
           const {data:{continent, country, regionName, city }} = await axios.get(`http://ip-api.com/json/${ipValue}?fields=continent,country,regionName,city`);
           document.querySelector(".ip-information").insertAdjacentHTML("beforeend", `
                  <li>continent: ${continent}</li>
                  <li>country: ${country}</li>
                  <li>region: ${regionName}</li>
                  <li>city: ${city}</li>
            `)
        }
     } catch (error) {
       console.log(error);
     }
})


