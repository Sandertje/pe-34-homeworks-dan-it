const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  }
];

const hasMissingKeys = (obj, keys) => {
  return keys.find(e=>{
    if (obj.hasOwnProperty(e)) {
      return false;
    } else {
      return e;
    }
  })
}

const createElementOnPage = (array) => {
  const div = document.querySelector("#root");
  const elementKey = ["author", "name", "price"];
  array.map(element => {
    try {
      if (element.author && element.name && element.price) {
        div.insertAdjacentHTML("beforeend", `<ul> <li>author:${element.author}</li> <li>name:${element.name}</li> <li>price:${element.price}</li> </ul>`);
      } else {
        const missingKeys = hasMissingKeys(element, elementKey);
        throw new Error(`undefined element - ${missingKeys}`);
      }
    } catch (error) {
      console.error(error);
    }
  })
}
createElementOnPage(books);


