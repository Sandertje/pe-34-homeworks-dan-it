fetch("https://ajax.test-danit.com/api/swapi/films")
  .then(res => {
  const {status , ok} = res;
        if (status===200 && ok) {
          return res.json();
        }
  }
 ).then(data=> {
   const container = document.querySelector(".container")
   data.forEach(({episodeId, name, openingCrawl, characters, id}) => {
     container.insertAdjacentHTML("beforeend", `
     <div class="item">
        <p class="item-id">episode ${episodeId}</p>
        <p class="item-title">"${name}"</p>
        <p>${openingCrawl}</p>
        <ul id="films-${id}"></ul>
     </div>
     `)
     characters.forEach(charactersUrl => {
       document.querySelector(`.item-id`).insertAdjacentHTML("beforebegin",
         `<div class="middle">
               <div class="bar bar1"></div>
               <div class="bar bar2"></div>
               <div class="bar bar3"></div>
               <div class="bar bar4"></div>
               <div class="bar bar5"></div>
               <div class="bar bar6"></div>
               <div class="bar bar7"></div>
               <div class="bar bar8"></div>
             </div>
             `)
       fetch(`${charactersUrl}`).then(res => {
         document.querySelector(".middle").remove();
         const {status , ok} = res;
         if (status===200 && ok) {
           return res.json()
         }
       }).then(({name}) => {
         document.querySelector(`#films-${id}`).insertAdjacentHTML("beforeend", `
         <li>${name}</li>
         `)
       })
         .catch(error => console.log(error));
     })
   })
  }).catch(error => console.log(error));

