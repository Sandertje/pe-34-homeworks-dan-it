// function sum(a,b){
//   return a+b;
// }
//
// console.log(sum(5,6));


// function arg(){
//   return arguments.length;
// }
// console.log(arg("Alice","Bob","Alex"))


// function count(start, end) {
//   if (start > end){
//     console.log("⛔️ Ошибка! Счёт невозможен.")
//   } else if (start === end ){
//     console.log("⛔️ Ошибка! Нечего считать")
//   } else {
//     console.log("🏁 Отсчёт начат.")
//     for (let i = start; i <= end; i++ ){
//       console.log(i)
//     }
//   console.log("✅ Отсчёт завершен.")
//   }
// }
//
//
// count(1, 9);


// function countAdvanced(start, end, div) {
//    if (arguments.length !== 3) {
//      throw new Error ("переайте 3 аргумента")
//    }
//
//    if (
//      isNaN (start)
//      || isNaN (end)
//      || isNaN(div)
//      || typeof start !== 'number'
//      || typeof end !== 'number'
//      || typeof div !== 'number'
//    ) {
//      throw new Error ("Один из аргументов не является допустимым числом")
//    }
//    if (start > end) {
//      console.log("⛔️ Ошибка! Счёт невозможен.")
//    } else if (start === end) {
//      console.log("⛔️ Ошибка! Нечего считать")
//    } else {
//      console.log("🏁 Отсчёт начат.")
//      for (let i = start; i <= end; i++) {
//      if (i % div === 0){
//      console.log(i)}
//      }
//      console.log("✅ Отсчёт завершен.")
//    }
// }
//
// countAdvanced(5,20,)


// let student = {
//   name: "Alex",
//   lastName: "Martian",
//   laziness: 4,
//   trick: 4
// };
//
// console.log(student);
//
// if (student.laziness >= 3 &&
//   student.laziness <=5 &&
//   student.trick <=4) {
//   console.log( `Студент ${student.name} ${student.lastName} отправлен на пересдачу`);
// }

//  let danItStudent = {
//    name: "Alex",
//    lastName: "Martian",
//    homeWorkQuantity: 10
//  };
//
// console.log(danItStudent);
//
// // let info = prompt("Что вы хотите узнать о студенте?");
// //
// // console.log(danItStudent [info])
//
// let prop = prompt(`Какое свойство вы хотите изменить?`);
// let newValue = prompt(`На какое значение?`);
//
// if (danItStudent.hasOwnProperty(prop)) {
//   danItStudent[prop] = newValue;
// }
//
//
// console.log(danItStudent);

const user = {
  firstName: 'Walter',
  lastName: 'White',
  job: 'Programmer',
  pets: {
    cat: 'Kitty',
    dog: 'Doggy',
  },
};


for (let key in user) {
  if (typeof key === `object`) {
    for (let key2 in user[key]) {
      console.log(user[key][key2]);
    }
  } else {
    console.log(user[key]);
  }
}

for (let key in user) {
  if (typeof user[key] === 'object') {
    for (let key2 in user[key]) {
      if (user[key].hasOwnProperty(key2)) {
        console.log(user[key][key2]);
      }
    }
  } else {
    console.log(user[key]);
  }
}
