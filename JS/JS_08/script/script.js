const enterPrice = document.querySelector(".elem");
const foundation = document.querySelector(".value");
const error = document.querySelector(".error");


enterPrice.addEventListener("focus", () =>  {
  enterPrice.classList.add("border");
})

enterPrice.addEventListener("blur", (e) =>{

  enterPrice.classList.remove("border");
  enterPrice.classList.add("color");

  const price = e.target.value;
  if (+price > 0) {
    let div = document.createElement('div');
    div.className = "block-price";
    foundation.before(div);

    let button = document.createElement("button");
    button.className = "remove-list";
    button.prepend(`x`);
    div.prepend(button);

    let span = document.createElement('span');
    span.className = "price-list";
    span.prepend(`Текущая цена: ${e.target.value}`)
    div.prepend(span);

    button.addEventListener("click", ()=> {
      div.remove();
      enterPrice.value = "";
    })
    enterPrice.classList.remove("error-border");
    const errorMes = document.querySelector(".error");
    errorMes.innerHTML = "";
  }
  else if (error.innerText !== "Please enter correct price" ) {
    enterPrice.classList.add("error-border");
    error.innerText = "Please enter correct price"
  }
})



