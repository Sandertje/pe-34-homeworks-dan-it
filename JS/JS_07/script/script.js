const item = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const showList = (item, parameter = document.body) => {
  let addList = document.createElement(`ul`);
  item.map(num => {
    return `<li>${num}</li>`;
  }).forEach(li => {
    addList.insertAdjacentHTML(`beforeend`, li)
  });
  parameter.append(addList);
}
showList(item);

