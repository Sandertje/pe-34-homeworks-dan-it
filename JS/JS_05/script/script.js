function createNewUser() {
  return {
    firstName: prompt("Enter your name"),
    lastName: prompt("Enter your lastname"),
    birthday: prompt("Enter your birthday (dd.mm.yyyy)"),
    getLogin() {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
    getAge() {
      const now = Date.now();
      const year = this.birthday.slice(6,10);
      const month = this.birthday.slice(3,5);
      const day = this.birthday.slice(0,2);
      const changeBirthday = new Date(`${year},${month},${day}`)
      const calcBirthday = now - changeBirthday;
      return new Date(calcBirthday).getFullYear() - 1970;
    },
    getPassword() {
      return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6, 10)
    },
  }
}
const newUser = createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());



