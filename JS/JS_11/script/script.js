const buttons = Array.from(document.querySelector(".btn-wrapper").children)

document.addEventListener('keydown', (e) => {
    buttons.forEach(element =>{
      if (element.dataset.value === (e.key).toUpperCase() || element.dataset.value === e.key) {
        buttons.forEach(e =>{
          e.classList.remove("blue-background")
        })
        element.classList.add("blue-background")
    }
      })
})

