const tabsList = document.querySelector(".tabs")
const tabsChildren = Array.from(tabsList.children);
tabsList.addEventListener("click",(e) => {
  let target = e.target;
  hideText(target);
})

const hideText = (i) => {
  tabsChildren.forEach( (e) => {
    e.classList.remove("active");
  });
  i.classList.add("active");

  const tabsChildrenIndexActive = tabsChildren.findIndex(e => e.classList.contains("active"));
  const tabsContentChildren = Array.from(document.querySelector(".tabs-content").children);
  tabsContentChildren.forEach(e => {
    if (+e.dataset.indexNumber !== tabsChildrenIndexActive) {
    e.classList.add("hide")
    } else {
      e.classList.remove("hide")
    }
  })
}




// const tabs = Array.from(document.querySelectorAll(".tabs-title"))
// tabs.forEach((e) => {
//   e.addEventListener("click", (item) => {
//     let active = document.querySelectorAll(".active");
//     for(let elem of active) {
//       elem.classList.remove('active');
//     }
//     item.target.classList.add('active');
//
//     const tabsChildren = Array.from(document.querySelector(".tabs").children);
//     const tabsChildrenActive = tabsChildren.findIndex(e => e.classList.contains("active"));
//     const tabsContentChildren = Array.from(document.querySelector(".tabs-content").children);
//     const result = tabsContentChildren.find((e,index) => tabsChildrenActive === index);
//     let hide = document.querySelector(".tabs-content").children;
//     for (let elem of hide) {
//     elem.classList.add("tabs-content-item");
//     }
//     result.classList.remove("tabs-content-item");
// })
// })
//



//
// switch (activeIndex) {
//   case 0: break;
//   case 1:
//     peoplePictures.forEach(e => {
//       e.classList.remove("content-about-block-list-img-active")
//     })
//     peoplePictures[activeIndex].classList.add("content-about-block-list-img-active")
// }
// }
