const fatherElem= document.querySelector(".password-form");

fatherElem.addEventListener("click", (event) => {
  let target = event.target;
  if (target.tagName !== "I") return;
  switchPassword(target);
});

const switchPassword = (i) => {
  const input = i.previousElementSibling;
  if (input.type === "password") {
    input.type = "text";
    i.classList.remove("fa-eye");
    i.classList.add("fa-eye-slash");
  } else {
    input.type = "password";
    i.classList.remove("fa-eye-slash");
    i.classList.add("fa-eye");
  }
};

const button = document.querySelector(".btn");
const pas = document.querySelector("#password");
const pasConfirm = document.querySelector("#confirm-password");
const errorMes = document.querySelector(".error");

button.addEventListener("click", () => {
  if (pas.value === pasConfirm.value && pas.value !== "" && pasConfirm.value !== "" ) {
    alert("You are welcome");
    errorMes.innerText = "";
  } else if (pas.value === "" || pasConfirm.value === "") {
    errorMes.innerText = "Введите пароль";
  } else {
   errorMes.innerText = "Нужно ввести одинаковые значения";
  }
}, passive = true);






