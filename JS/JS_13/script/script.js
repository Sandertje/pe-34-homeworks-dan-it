window.onload = () => {

  if (localStorage.getItem("backgroundColor") !== null && localStorage.getItem("colorText") !== null) {
    let backgroundColor = localStorage.getItem("backgroundColor");
    let colorText = localStorage.getItem("colorText");
    document.getElementsByTagName("body")[0].style.background = backgroundColor;
    Array.from(document.querySelectorAll(".nav-menu-link")).forEach( (e)=> {
      e.style.color = colorText;
    })
    Array.from(document.querySelectorAll(".nav-menu-item")).forEach((e) => {
      e.style.borderColor = colorText
    })
  }

  document.querySelector('.nav-change').addEventListener("click", () => {

    if (document.getElementsByTagName("body")[0].style.background === "pink") {

        document.getElementsByTagName("body")[0].style.background = "";
        Array.from(document.querySelectorAll(".nav-menu-link")).forEach( (e)=> {
        e.style.color = ""}
        );
        Array.from(document.querySelectorAll(".nav-menu-item")).forEach((e) => {
        e.style.borderColor = ""}
        );
    } else {
      document.getElementsByTagName("body")[0].style.background = "pink";
      Array.from(document.querySelectorAll(".nav-menu-link")).forEach( (e)=> {
        e.style.color = "#40E0D0";
      })
      Array.from(document.querySelectorAll(".nav-menu-item")).forEach((e) => {
        e.style.borderColor = "#40E0D0"
      })
      localStorage.setItem("backgroundColor", "pink");
      localStorage.setItem("colorText", "#40E0D0");
    }
  }
  )
}

