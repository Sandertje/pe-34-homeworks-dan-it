$(document).ready(() => {
  $(".hero-list").on("click", (e)=> {
    e.preventDefault()
    let href  = $(e.target).attr('href');
    let top = $(href).offset().top;
    $('html, body').animate({scrollTop: top}, 1500);
  })

  window.addEventListener('scroll', () => {
    if (pageYOffset>window.outerWidth) {
      let button = $("header button");
      button.addClass("hero-button-active");
      button.removeClass("hero-button");
    }
  });

  $("header button").click ( () => {
    $('html, body').animate({scrollTop: 0}, 1500);
  })

  $(".content-posts-button-hide").click( ()=> {
    $(".content-posts-card").slideToggle(2000)
  })
})


