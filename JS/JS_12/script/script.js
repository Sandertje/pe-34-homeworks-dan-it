const images = [
  "./img/1.jpg",
  "./img/2.jpg",
  "./img/3.jpg",
  "./img/4.jpg",
];
let currentSlide = 0;
const imagesContainer = document.querySelector(".images-wrapper");
const imagesElem = document.querySelector(".images-wrapper img");
imagesContainer.insertAdjacentHTML("afterend", `<div class="buttons">
  <button class="buttons-stop">Прекратить</button>
  <button class="buttons-continue">Возобновить показ</button>
</div>`);

const rootChangePic = () => {
  currentSlide++;
  imagesContainer.classList.add("fade-out");
  setTimeout(() => {
    imagesContainer.classList.remove("fade-out");
    imagesElem.src = images[currentSlide];
    imagesContainer.classList.add("fade-in");
  }, 1500);
  setTimeout(() => {
    imagesContainer.classList.remove("fade-in")
  }, 2000)
}

const changePic = () => {
  if (currentSlide < images.length - 1) {
    rootChangePic()
  } else {
    currentSlide = -1;
    rootChangePic()
  }
}

let intervalPic = setInterval(changePic, 3000);

document.querySelector(".buttons-stop").addEventListener("click", () => {
  clearInterval(intervalPic);
});
document.querySelector(".buttons-continue").addEventListener("click", () => {
  clearInterval(intervalPic);
  rootChangePic()
  intervalPic = setInterval(changePic, 3000);
});


console.log(images.length)
